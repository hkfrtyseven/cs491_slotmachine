'''
FILE:           player.py
DESCRIPTION:    Player class module for slotmachine.py
AUTHOR:         Matt Facque
TEST for docker
'''

class Bank():
    def __init__(self, money_pool):
        self.money_pool = money_pool
        self.money_won = 0
        self.money_lost = 0
        self.total_wins = 0
        self.total_losses = 0

    def get_money_pool(self):
        return self.money_pool

    def add_to_money_pool(self, add_money):
        self.money_pool += add_money

    def take_from_money_pool(self, take_money):
        self.money_pool -= take_money

    def add_money_won(self, winnings):
        self.money_won += winnings

    def get_money_won(self):
        return self.money_won

    def add_money_lost(self, losses):
        self.money_lost += losses

    def get_money_lost(self):
        return self.money_lost

    def add_to_money_won(self, winnings):
        self.money_won += winnings
    
    def get_total_wins(self):
        return self.total_wins

    def get_total_losses(self):
        return self.total_losses

    def add_win(self):
        self.total_wins += 1

    def add_loss(self):
        self.total_losses += 1

class Player():
    def __init__(self, name, money_pool):
        self.name = name
        self.Bank = Bank(money_pool)

    def get_name(self):
        return self.name
    
    def set_name(self, new_name):
        self.name = new_name

    def player_debrief(self):
        print("Your total pulls won:  " + str(self.Bank.get_total_wins()))
        print("Your total winnings:   " + str(self.Bank.get_money_won()))
        print("Your total pulls lost: " + str(self.Bank.get_total_losses()))
        print("Your total losses:     " + str(self.Bank.get_money_lost()))
        return True
