'''
FILE:           machine.py
DESCRIPTION:    Module for slotmachine and dials classes for slotmachine.py
AUTHOR:         Matt Facque
NOTE:           Imitate very first slot machine (Liberty Bell 1894)
comment
'''
import random

class Reel:
    def __init__(self):
        self.symbol = None

    # Reel symbols (6)
    SYMBOLS = ["DIAMOND","HEART","SPADE","HORSESHOE","STAR","LIBERTYBELL"]

    def get_Symbol(self):
        return self.symbol
    
    def assign_Symbol(self, random_choice):
        self.symbol = self.SYMBOLS[random_choice]

class Machine:
    def __init__(self):
        self.reel1 = Reel()
        self.reel2 = Reel()
        self.reel3 = Reel()
        self.final = []
        self.scoreOut = None

    def randomize_Symbol(self):
        random_num = random.randint(0,5)
        return random_num
    
    def pull_Arm(self):
        self.final.clear()
        self.reel1.assign_Symbol(self.randomize_Symbol())
        self.final.append(self.reel1.get_Symbol())
        self.reel2.assign_Symbol(self.randomize_Symbol())
        self.final.append(self.reel2.get_Symbol())
        self.reel3.assign_Symbol(self.randomize_Symbol())
        self.final.append(self.reel3.get_Symbol())

    def output_Reels(self):
        print("---------------------------------------------")
        for x in range(2):
            self.format_Output(self.final[x])
        self.format_Output_nl(self.final[2])
        print("---------------------------------------------")

    def format_Output(self, input_symbol):
        if (input_symbol == "DIAMOND"):
            print("|   " + input_symbol + "   |", end='')
        elif (input_symbol == "HEART"):
            print("|    " + input_symbol + "    |", end='')
        elif (input_symbol == "SPADE"):
            print("|    " + input_symbol + "    |", end='')
        elif (input_symbol == "HORSESHOE"):
            print("|  " + input_symbol + "  |", end='')
        elif (input_symbol == "STAR"):
            print("|     " + input_symbol + "    |", end='')
        elif (input_symbol == "LIBERTYBELL"):
            print("| " + input_symbol + " |", end='')

    def format_Output_nl(self, input_symbol):
        if (input_symbol == "DIAMOND"):
            print("|   " + input_symbol + "   |")
        elif (input_symbol == "HEART"):
            print("|    " + input_symbol + "    |")
        elif (input_symbol == "SPADE"):
            print("|    " + input_symbol + "    |")
        elif (input_symbol == "HORSESHOE"):
            print("|  " + input_symbol + "  |")
        elif (input_symbol == "STAR"):
            print("|     " + input_symbol + "    |")
        elif (input_symbol == "LIBERTYBELL"):
            print("| " + input_symbol + " |")
        
    def winCon(self):
        reel1 = self.reel1.get_Symbol()
        reel2 = self.reel2.get_Symbol()
        reel3 = self.reel3.get_Symbol()

        # Check win conditions
        if ((reel1 == "HORSESHOE" and reel2 == "HORSESHOE" and reel3 == "STAR") or
            (reel1 == "HORSESHOE" and reel2 == "STAR" and reel3 == "HORSESHOE") or
            (reel1 == "STAR" and reel2 == "HORSESHOE" and reel3 == "HORSESHOE")):
            self.scoreOut = 10
            return self.scoreOut
        elif ((reel1 == "HORSESHOE" and reel2 == "HORSESHOE") or
            (reel1 == "HORSESHOE" and reel3 == "HORSESHOE") or
            (reel2 == "HORSESHOE" and reel3 == "HORSESHOE")):
            self.scoreOut = 5
            return self.scoreOut
        elif (reel1 == "SPADE" and reel2 == "SPADE" and reel3 == "SPADE"):
            self.scoreOut = 20
            return self.scoreOut
        elif (reel1 == "DIAMOND" and reel2 == "DIAMOND" and reel3 == "DIAMOND"):
            self.scoreOut = 30
            return self.scoreOut
        elif (reel1 == "HEART" and reel2 == "HEART" and reel3 == "HEART"):
            self.scoreOut = 40
            return self.scoreOut
        elif (reel1 == "LIBERTYBELL" and reel2 == "LIBERTYBELL" and reel3 == "LIBERTYBELL"):
            self.scoreOut = 50
            return self.scoreOut
        else:
            return 0
