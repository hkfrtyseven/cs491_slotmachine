'''
FILE:           slotmachine_testing.py
DESCRIPTION:    File to contain unit and integration testing for slotmachine.py
AUTHOR:         Matt Facque
'''

import slotmachine
import player
import machine
import unittest
import unittest.mock
from unittest.mock import patch

class Test_SlotMachine(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        print('setupClass')

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('Setup')
        self.player1 = player.Player("test",500)
        self.player2 = player.Player("othertest",100)
        self.reel1 = machine.Reel()
        self.reel2 = machine.Reel()
        self.slotmachine = machine.Machine()
        self.bank1 = player.Bank(500)
        self.bank2 = player.Bank(100)
        self.slotmachine1 = machine.Machine()
        self.slotmachine2 = machine.Machine()

    def tearDown(self):
        print('tearDown\n')

    # Test the test 
    def test_test_function(self):
        hello_world = slotmachine.test_function()
        self.assertEqual(hello_world, "Hello, World!")

    # Test debrief
    def test_Debrief(self):
        pass_through = self.player1.player_debrief()
        self.assertEqual(True, pass_through)

    # Test welcome message
    def test_welcome_message(self):
        self.assertTrue(slotmachine.welcome_message())

    # Test player name
    def test_PlayerName(self):
        self.assertEqual("test", self.player1.get_name())
        self.assertEqual("othertest", self.player2.get_name())

    # Test player starting money
    def test_PlayerMoney(self):
        self.assertEqual(500, self.bank1.get_money_pool())
        self.assertNotEqual(500, self.bank2.get_money_pool())
        self.assertEqual(100, self.bank2.get_money_pool())

    # Test player gets money after win
    def test_Winnings(self):
        self.bank1.add_to_money_pool(100)
        self.assertEqual(600, self.bank1.money_pool)

        self.bank2.add_to_money_pool(1000)
        self.assertEqual(1100, self.bank2.money_pool)

    # Test player loses money after loss
    def test_Losses(self):
        self.bank1.take_from_money_pool(100)
        self.assertEqual(400, self.bank1.money_pool)

        self.bank2.take_from_money_pool(100)
        self.assertEqual(0, self.bank2.money_pool)

    # Test getter method
    def test_GetMoney(self):
        self.assertEqual(500, self.bank1.get_money_pool())

    # Test player round wins increments
    def test_WinRound(self):
        self.bank1.add_win()
        self.assertEqual(1, self.bank1.total_wins)

        self.bank2.add_win()
        self.bank2.add_win()
        self.bank2.add_win()
        self.bank2.add_win()
        self.assertEqual(4, self.bank2.total_wins)

    # Test player round losses increments
    def test_LoseRound(self):
        self.bank1.add_loss()
        self.bank1.add_loss()
        self.assertEqual(2, self.bank1.total_losses)

    # Test getter method
    def test_GetWins(self):
        self.bank1.total_wins = 1
        self.assertEqual(1, self.bank1.get_total_wins())

        self.bank2.total_wins = 50
        self.assertEqual(50, self.bank2.get_total_wins())

    # Test player money winnings record
    def test_getWinnings(self):
        self.bank1.add_money_won(100)
        self.assertEqual(100, self.bank1.money_won)

        self.bank2.add_money_won(10000)
        self.assertEqual(10000, self.bank2.money_won)

    # Test player money losses record
    def test_getLosses(self):
        self.bank1.add_money_lost(100)
        self.assertEqual(100, self.bank1.money_lost)

    # Test Symbols DIAMOND, HEART, SPADE, HORSESHOE, STAR, LIBERTYBELL
    def test_ReelSymbol(self):
        symbol_0 = self.reel1.SYMBOLS[0]
        self.assertEqual("DIAMOND", symbol_0)

        symbol_3 = self.reel1.SYMBOLS[3]
        self.assertEqual("HORSESHOE", symbol_3)
    
    # Test symbol reel functions
    def test_GetReelSymbol(self):
        self.reel1.symbol = "DIAMOND"
        self.assertEqual("DIAMOND", self.reel1.get_Symbol())

    # Test assign Symbol function
    def test_AssignSymbol(self):
        self.reel2.assign_Symbol(2)
        self.assertEqual("SPADE", self.reel2.symbol)

    # Test randomizer
    def test_Random(self):
        number = self.slotmachine.randomize_Symbol()
        print("Random int: " + str(number))
        self.assertLessEqual(number, 5)
        self.assertGreaterEqual(number, -1)
    
    # Test yes or no function
    @patch('builtins.input', side_effect=['y','n'])
    def test_yes_or_no(self, mock_input):
        question = "Test question?"

        yes_bool = slotmachine.yes_or_no(question)
        self.assertEqual(True, yes_bool)

        no_bool = slotmachine.yes_or_no(question)
        self.assertEqual(False, no_bool)
    
    # Test player name enter
    @patch('builtins.input', return_value="matt")
    def test_playerName(self, mock_input):
        player_name = slotmachine.get_player_name()
        self.assertEqual("matt", player_name)

    # Test bank
    def test_check_Player_bank(self):
        false = slotmachine.check_player_bank(10)
        true = slotmachine.check_player_bank(0)

        self.assertEqual(False, false)
        self.assertEqual(True, true)

    # Test winCon
    def test_winCon(self):
        self.slotmachine1.reel1.assign_Symbol(3)
        self.slotmachine1.reel2.assign_Symbol(3)
        self.slotmachine1.reel3.assign_Symbol(4)

        win_value = self.slotmachine1.winCon()
        self.assertEqual(10, win_value)

        #print(self.slotmachine.reel1.get_Symbol())
        #print(self.slotmachine.reel2.get_Symbol())
        #print(self.slotmachine.reel3.get_Symbol())

        self.slotmachine1.reel3.assign_Symbol(0)
        win_value = self.slotmachine1.winCon()
        self.assertEqual(5, win_value)

    # Test Reels with machine
    def test_Reels_by_Machine(self):
        self.slotmachine1.reel1.assign_Symbol(0)
        symbol = self.slotmachine1.reel1.SYMBOLS[0]
        self.assertEqual(self.slotmachine1.reel1.get_Symbol(), symbol)

        self.slotmachine2.reel1.assign_Symbol(2)
        symbol = self.slotmachine2.reel1.SYMBOLS[3]
        self.assertNotEqual(self.slotmachine2.reel1.get_Symbol(), symbol)

    # Test pull Arm
    def test_Pull_arm(self):
        self.slotmachine2.pull_Arm()

        self.assertTrue(self.slotmachine2.reel1.symbol)
        self.assertTrue(self.slotmachine2.reel2.symbol)
        self.assertTrue(self.slotmachine2.reel3.symbol)

        self.assertEqual(3, len(self.slotmachine2.final))

    # Test Bank through Player
    def test_get_Player_money(self):
        self.assertEqual(500, self.player1.Bank.money_pool)
        self.assertNotEqual(500, self.player2.Bank.money_pool)

    # Test Bank add wins through Player
    def test_Add_win(self):
        self.player1.Bank.add_win()

        self.assertEqual(1, self.player1.Bank.get_total_wins())

    # Test Bank add loss
    def test_Add_loss(self):
        self.player2.Bank.add_loss()

        self.assertEqual(1, self.player2.Bank.get_total_losses())

if __name__ == '__main__':
    unittest.main()