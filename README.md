# cs491_slotmachine

Simple slot machine python app

This is a simple slot machine game based on the original slot machine, Liberty Bell.
Liberty Bell was created in 1894 by Charles Fey.  The slot machine has three reels
each with a horseshoe, star, diamond, heart, spade and a cracked bell, the the liberty bell.
The cost to play the slot machine was 5 cents.  The largest payout was 50 cents.
In this game, the user starts with a 100 dollars and the cost to play is 5 dollars.

# Winning Combinations
2 horeshoes           - 5 dollars  
2 horseshoes + 1 star - 10 dollars  
3 spades              - 20 dollars  
3 diamonds            - 30 dollars  
3 hearts              - 40 dollars  
3 liberty bells       - 50 dollars  

This project was developed for CS 491 - Testing and DevOps in the department of Computer Science
and Engineering, University of Nevada, Reno.  The project is hosted on gitlab with source control by git,
testing through unittest and testing coverage with coverage.py, and deployment through docker.


------------------------------------------------------------------------------------
# Setup
# Pull from gitlab
mkdir "your-chosen-directory"
cd "your-chosen-directory"
git clone https://gitlab.com/hkfrtyseven/cs491_slotmachine.git

# The project is designed in python 3
python --version

If your python version is not 3, upgrade to python 3.
# Python On linux:
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.9

# Python On Windows
Download python for windows: https://www.python.org/downloads/

# Python On Mac
Download the mac installer: https://www.python.org/downloads/release/python-3104/

------------------------------------------------------------------------------------
# Install coverage.py
All packages that the Liberty Bell slotmachine app requires are installed with 
the python install.  However, to check testing coverage, the project needs 
coverage.py.

pip install coverage

------------------------------------------------------------------------------------
# Usage
python3 slotmachine.py

------------------------------------------------------------------------------------
# Test/Check Test Coverage
The testing for the Liberty Bell slotmachine app is conducted with unittest and the
test coverage is conducted by coverage.py

# Usage
python3 slotmachine_testing.py

# Usage w/ coverage.py
coverage run -m unittest slotmachine_testing.py

# Check Test Coverage Report
coverage report

------------------------------------------------------------------------------------
# Run w/ Docker Image
Liberty Bell slot machine app is also deployed with docker and the image is hosted on Gitlab.

# Docker on Linux
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Docker on Windows
Download Docker for windows: https://docs.docker.com/desktop/windows/install/

# Docker on Mac
Download Docker for mac: https://docs.docker.com/desktop/mac/install/
Note:  There is a question about intel/apple chip, you need to check your hardware  
apple menu -> about this mac

------------------------------------------------------------------------------------
To the pull the latest version of the Libery Bell slot machine app, you need to run the image
that is hosted on Gitlab.

# Usage
docker run -it registry.gitlab.com/hkfrtyseven/cs491_slotmachine:latest

