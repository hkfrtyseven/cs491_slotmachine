'''
FILE:           slotmachine.py
DESCRIPTION:    Main file for slot machine app
AUTHOR:         Matt Facque
Update comment for pipeline
test comment
'''

import player
import machine

player_question = "Would you like to keep playing?"

def test_function():
    test = "Hello, World!"
    return test

def welcome_message():
    return '''Hello, thanks for playing the Liberty Bell Slot Machine.\nThis is the first slot machine ever made, invented in 1894 by Charles Fey.\nI hope you enjoy!\nYou start with $100, let's see how much you leave with!\n'''

def yes_or_no(question):
    reply = str(input(question+' (y/n): ')).lower().strip()
    if reply[:1] == 'y':
        return True
    if reply[:1] == 'n':
        return False
    else:
        return yes_or_no(question)

def get_player_name():
    return input("What is your name? ")

def check_player_bank(dollars):
    if (dollars < 5):
        return True
    else:
        return False

if __name__ == '__main__':
    #  Introduce Player
    message = welcome_message()
    print(message)

    name = get_player_name()

    player1 = player.Player(name,100)
    libertybell = machine.Machine()
    playing = True

    #  Game loop
    while(playing):
        correct = True

        print("You have " + str(player1.Bank.get_money_pool()) + " dollars remaining.\n")
        game_over = check_player_bank(player1.Bank.get_money_pool())
        if (game_over):
            print("Seems you've run outta cash.\n")
            break;
        else:
            player1.Bank.take_from_money_pool(5)

        #  Pull slot machine arm
        libertybell.pull_Arm()

        #  WAIT
        #  Finalize slot machine dials
        libertybell.output_Reels()

        winVal = libertybell.winCon()
        if (winVal == 0):
        #   Player loses
            player1.Bank.add_loss()
            player1.Bank.add_money_lost(5)
            print("Better luck next time!\n")
        else:
        #   Player wins
            player1.Bank.add_win()
            player1.Bank.add_money_won(winVal)
            player1.Bank.add_money_lost(-1 * winVal)
            player1.Bank.add_to_money_pool(winVal)
            print("You won " + str(winVal) + "!\n")

        keep_playing = yes_or_no(player_question)
        if (not keep_playing):
            playing = False
        else:
            playing = True

    #  Player debrief
    closed = player1.player_debrief()

    #  Thanks for playing!
    print(player1.get_name() + ", thanks for playing!")
